new Vue({
  el: '#exercise',
  data: {
    isHighlight: true,
    classX: "",
    classY: "",
    classZ: "silver",
    isClassZ: "",
    dimension: "",
    progress: 0
  },
  methods: {
    startEffect: function() {
      this.isHighlight = !this.isHighlight
    },
    setIntervalA: function() {
      var that = this
      that.progress = 0
      clearInterval()
          setInterval(function(){
            if (that.progress < 200) {
              that.progress += 10
              console.log(that.progress)
            }
            
            
          }, 100);
        
    }
  
  },
  computed: {
    dimensionC: function(){
      console.log(this.dimension)
      return {height: this.dimension + "px", width: this.dimension + "px"}
    },
    progressBar: function(){
      return {"height": "20px", "width": (this.progress)+"px", "backgroundColor": "green"}
    }

  }
});
