new Vue({
    el: '#exercise',
    data: {
        value: 0,
        resetTime: 5000
    },
    computed: {
        result: function() {
            return this.value < 37 ? "not there yet" : ""
        }
    },
    watch: {
        result: function() {
            var mv = this;
            setTimeout(function(){
                mv.value = 0;
            }, mv.resetTime)
        }
    }
});